from __future__ import division

import numpy as np
import tensorflow as tf
from keras import backend as K
import cv2


def non_max_suppression(detections: list, confidence=0.5, num_classes=80, nms_conf=0.4):
    """ Remove all detections which objectness score under confidence, then do NMS.

    subject our output to objectness score thresholding and Non-maximal suppression
    Non-maximal suppression part use function: tf.image.non_max_suppression

    :param detections: output of modle
    :param confidence: objectness score threshold
    :param num_classes: number of classes
    :param nms_conf: the NMS IoU threshold
    
    :return:
        pred_after_nms_list: all detections after NMS for each image.
    """

    assert len(detections) == 3
    assert all(type(detection) == np.ndarray for detection in detections)

    batch_size = detections[0].shape[0]
    assert all(detection.shape[0] == batch_size for detection in detections)

    num_of_bounding_box_attributes = num_classes + 5

    # reshape to batch*all_grid*bounding_box_attributes
    detections = [detection.reshape(batch_size, -1, num_of_bounding_box_attributes) for detection in detections]
    prediction = np.concatenate(detections, axis=1)

    # this part maybe use tf.boolean_mask
    # but, how to keep dimension is a problem.
    conf_mask = prediction[..., 4] > confidence  # get mask for object score larger than confidence parameter

    # change shape of bounding box info from (centre_x, centre_y, H, W) to
    # (left_top_x, left_top_y, right_bottom_x, right_bottom_y)
    box_corner = np.concatenate([prediction[..., 0:1] - prediction[..., 2:3] / 2,  # left_top_x
                                 prediction[..., 1:2] - prediction[..., 3:4] / 2,  # left_top_y
                                 prediction[..., 0:1] + prediction[..., 2:3] / 2,  # right_bottom_x
                                 prediction[..., 1:2] + prediction[..., 3:4] / 2],  # right_bottom_y
                                axis=-1)
    assert len(box_corner.shape) == 3

    # all classes score is the output of softmax, NOT ONE-HOT!!
    # use tf.nn.top_k to get max value and its index
    # change classes scores from one-hot to indices
    # as return is indices in list, so start from 0
    class_scores = np.amax(prediction[..., 5:], axis=-1, keepdims=True)
    class_indices = np.argmax(prediction[..., 5:], axis=-1)[..., np.newaxis]  # add new axis to keep dimension
    assert len(class_scores.shape) == 3
    assert len(class_indices.shape) == 3

    # concatenate box corner, objectness score, class score and class index together
    predict_transformed = np.concatenate([box_corner, prediction[..., 4:5], class_scores, class_indices], axis=-1)
    assert predict_transformed.shape[-1] == 7  # third axis should equal 4+1+1+1

    pred_after_nms_list = []

    # loop all batches(each batch is an image) for NMS
    for batch in range(batch_size):
        img_pred = predict_transformed[batch]  # image detection tensor
        img_conf_mask = conf_mask[batch]  # mask of this image

        img_pred_masked = img_pred[img_conf_mask, :]  # apply mask to ndarray
        assert len(img_pred_masked.shape) == 2

        classes_list = np.unique(img_pred_masked[:, -1])  # get all class index detected in this image

        selected_detection_list = []  # hold all detections after NMS for each class

        for class_index in classes_list:
            class_mask = img_pred_masked[..., -1] == class_index  # get class's mask

            img_pred_of_class = img_pred_masked[class_mask, :]  # get hold info of this class
            assert len(img_pred_of_class.shape) == 2

            img_pred_of_class_tensor = tf.convert_to_tensor(img_pred_of_class, dtype=tf.float32)

            selected_indices = tf.image.non_max_suppression(img_pred_of_class_tensor[:, :4],
                                                            scores=img_pred_of_class_tensor[:, -2],
                                                            max_output_size=1)  # NMS
            selected_boxes = tf.gather(img_pred_of_class_tensor, selected_indices)  # get hold info of selected by NMS
            selected_detection = K.eval(selected_boxes)

            selected_detection_list.append(selected_detection)  # append this class's detection in list

        # concatenate all detections after NMS to one array
        pred_after_nms = np.concatenate(selected_detection_list, axis=0) if selected_detection_list else None

        pred_after_nms_list.append(pred_after_nms)

    return pred_after_nms_list


def draw_bounding_box(results: list, img_paths: list, classes: list):
    """ Draw bounding box and label of objects on all original images.

    :param results: predictions of Convolution network though the NMS
    :param img_paths: string list of original image paths.
    :param classes: string list of classes' name.

    :return: nothing.
    """

    assert len(results) == len(img_paths)

    for detections, image_path in zip(results, img_paths):

        if detections is None: continue  # if there is no object detected from image, skip it.

        # parameters for print label text.
        text_font = cv2.FONT_HERSHEY_SIMPLEX
        text_height = cv2.getTextSize('label:1.0', fontFace=text_font, fontScale=0.5, thickness=1)[0]
        text_coordinate_correction = np.array([0, text_height[1]]) + 2

        drawn_bounding_image = cv2.imread(image_path)
        scale_factor = np.array(drawn_bounding_image.shape[:2])[::-1]  # get H and W of image and reverse it to W*H
        scale_factor = np.append(scale_factor, scale_factor) / 416  # get scale factor for both corners of bounding box

        # # convert bounding box coordinates to original image size.
        # detections[..., :4] = np.round(detections[..., :4] * scale_factor).astype(np.int32)

        for detection in detections:
            # draw bounding box and label for each prediction.
            # get coordinates of Left-top and Right-bottom.
            bounding_box_coordinate = np.round(detection[:4] * scale_factor).astype(np.int32)

            # random choice a color which has same RGB value for bounding box and text.
            bounding_box_color: list = np.random.randint(125, 255 + 1, size=(3,)).tolist()
            bounding_box_color[np.random.randint(3)] = 0
            # get descriptive str of detected object.
            bounding_box_text = classes[int(detection[-1])] + ":" + str(detection[-2])

            # draw bounding box
            drawn_bounding_image = cv2.rectangle(drawn_bounding_image,
                                                 tuple(bounding_box_coordinate[0:2]),
                                                 tuple(bounding_box_coordinate[2:4]),
                                                 color=bounding_box_color, thickness=2)

            # add descriptive str on left-top corner of bounding box.
            drawn_bounding_image = cv2.putText(drawn_bounding_image, bounding_box_text,
                                               org=tuple(bounding_box_coordinate[0:2] + text_coordinate_correction),
                                               fontFace=text_font, fontScale=0.5,
                                               color=bounding_box_color, thickness=1)
        else:
            file_extensions_index = image_path.rindex('.')
            output_image_path = image_path[:file_extensions_index] + "-yolo" + image_path[file_extensions_index:]

            cv2.imwrite(output_image_path, drawn_bounding_image)
