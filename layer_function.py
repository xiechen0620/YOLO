import numpy as np
from keras.layers import convolutional, LeakyReLU, BatchNormalization, Activation, ZeroPadding2D
from keras.layers import Add
import tensorflow as tf


def _num_counter(start=0):
    while True:
        start += 1
        yield start


_layer_counter = _num_counter()


def reset_layer_counter(get_layer_number=True):
    """ reset the layer counter and get laset layer's number.
    """
    global _layer_counter

    layer_number = next(_layer_counter) - 1
    _layer_counter = _num_counter()

    print("layer counter is reset.")

    if get_layer_number:
        return layer_number


def conv2d_with_bn(filters: int, kernel_size: int, stride: int, padding: int, activation: str, batch_normalize: int):
    """ Factory function for Lambda layer to merge convolution, BN and LeakyReLU activation layer together.

    As the guide of YOLOv3 implemented by PyTorch merge a convNet and a batch normalization
    as one layer, so I have to use this func to do this.

    :param filters: The number of filters used in this conv layer. Same as the dimension of output
    :param kernel_size: An int specifying the length of the 2D convolution window
    :param stride: An int specifying the stride length of the convolution
    :param padding: An int padding flag. Use padding or not.
    :param activation: A string of the active function used.
    :param bias: An boolean flag for convoluation layer for use bias in it or not.
    :param batch_normalize: An int BN flag. There is a BN layer following the convNet layer.
    :param layer_index: A number of this block order.

    :return: A function as a convNet layer with BN layer depends on flag.
    """

    # Check the activation
    active = LeakyReLU(alpha=0.1) if activation == "leaky" else "linear"
    bias = not batch_normalize
    layer_index = next(_layer_counter)

    def layer_func(input_tensor):
        """ Layer function as a Keras layer.

        check the batch_normalize, if value is 1, then add a BN layer after convNet layer;
        otherwise, only conv layer. 
        Then add an Activation layer for LeakReLU active function

        :param input_tensor: input tensor for layers
        :return: output tensor of this merged convolution layer.
        """

        conv_layer = convolutional.Conv2D(filters, kernel_size, strides=stride, padding="same",
                                          use_bias=bias, name="conv2d_" + str(layer_index))

        # Check padding flag and stride to decide use padding or not, and how padding when stride > 1.
        if padding == 1 and stride == 1:
            pass
        elif padding == 1 and stride != 1:
            pad = (kernel_size - 1) // 2
            input_tensor = ZeroPadding2D(padding=pad)(input_tensor)
            conv_layer.padding = "valid"
        else:
            # this case shouldn't exist in YOLOv3
            conv_layer.padding = "valid"

        mid_output = conv_layer(input_tensor)

        if batch_normalize:
            # If batch_normalize flag rise, add a BN layer after the convNet layer,
            # or just return the convNet layer's output.
            mid_output = BatchNormalization(name="batch_normalization_" + str(layer_index))(mid_output)

        # add Activation to conv or BN layer.
        activation_output = Activation(activation=active, name="activation_" + str(layer_index))(mid_output)

        return activation_output

    return layer_func


def bilinear_upsample(stride: int):
    """ Factory function for Lambda layer as a bilinear upsample layer

    Use tf.image.resize_images function to implement bilinear upsampling of Pytorch.

    :param stride:  the multiplier for the image height / width
    :return: A function as a bilinear upsample layer
    """

    def upsample_layer_func(image):
        """ A layer function as Keras layer

        Upsample input tensor by bilinear upsample.

        :param image: a channel last tensor as input of layer.
        :return: If images was 4-D, a 4-D float Tensor of shape [batch, new_height, new_width, channels].
                 If images was 3-D, a 3-D float Tensor of shape [new_height, new_width, channels]
        """

        image_size = np.array(image.get_shape().as_list())  # get the shape of tensor as array.
        new_image_size = np.int32(image_size[-3:-1] * stride)  # compute new size of tensor should be.
        new_image = tf.image.resize_images(image, new_image_size)  # use tf's function to bilinear upsampling.
        return new_image

    return upsample_layer_func


def bilinear_upsample_shape(stride: int):
    """ Factory function for Lambda layer to compute shape of output.

    As default output shape of Lambda layer is same as input's shape. 
    So I have to use a output_shape_function to get correct shape of layer output for Keras to create Graph.

    :param stride: Expansion multiple of height and width.
    :return: a function to get correct shape of output.
    """

    def upsample_layer_shape_func(input_shape):
        """ Compute output shape function.

        Take input's shape from Lambda layer, and retuen the output's shape for creating map.

        :param input_shape: the input's shape of Lambda layer.
        :return: a tuple. new shape of upsampled image. (B*new_H*new_W*C)
        """

        shape = np.array(input_shape)  # turn input_shape to array
        shape[-3:-1] *= stride  # comput new_H and new_W of new image
        return tuple(shape)

    return upsample_layer_shape_func


def predict_decode(inp_dim: int, anchors: np.ndarray, num_classes: int):
    """ Factory function for Lambda layer to get bounding box info and probability of each attribute.

    The value of channel axis(axis=-1) should be (5+number_of_classes)*number_of_anchor_boxes.
    In each block of anchors
        first two channels stand for x,y coordinate of centre point.
        next two channels stand for w,h value of bounding box.
        fifth channel stand for objectness score which means there is object detected in this grid box or not.
        the other channels stand for class scores which means the probability for each class.
    Using sigmoid on all channels, then follow the paper decode x,y coordinate and w,h value of bounding box.

    The coordinate of centreX and centreY would transfer to input image size axis.
    The size of bounding box would transfer to value based on input image size.

    The shape of input and output should be same.

    :param inp_dim: input image's height or width. get this parameter from 'net_info' - 'height'
    :param anchors: all anchors' size which used in this scale
    :param num_classes: number of classes which could be detected by this model.
    :return: decoded tensor as output of model
    """

    num_anchors = len(anchors)
    num_of_bounding_box_attributes = 5 + num_classes

    def layer_function(prediction: tf.Tensor):
        """ layer function to decode.

        :param prediction: a 'channel last' tensor, which means that shape of our output from convNet is (B*H*W*C)
        :return: decoded tensor as output of model
        """

        predict_shape_list = prediction.get_shape().as_list()
        assert len(predict_shape_list) == 4
        assert num_anchors * num_of_bounding_box_attributes == predict_shape_list[-1]
        predict_shape = tf.shape(prediction)  # get a shape type of input's shape to reshape back.

        input_dtype = prediction.dtype
        stride = inp_dim // predict_shape_list[1]
        grid_size = inp_dim // stride
        assert grid_size == predict_shape_list[1]

        # change shape to (B, H, W, number_of_anchors*num, number_of_bounding_box_attributes) for convenience.
        new_prediction = tf.reshape(prediction,
                                    shape=[-1, grid_size, grid_size, num_anchors, num_of_bounding_box_attributes])

        """
        split tensor channel into 4 parts:
        1. grid box centre point's coordinate(tx, ty)
        2. bounding box size(tw, th)
        3. Objectness score(po)
        4. Class score
        """
        """Decode centre_x and centre_y"""
        # sigmoid the centre_X, centre_Y
        centre_x_y = tf.sigmoid(new_prediction[..., :2])

        # Add the center offset
        # create the coordinates for all gride box
        # Question: I don't know why should plus 1 on each index...but could get correct result...
        x_y_offset = np.array(np.meshgrid(range(grid_size), range(grid_size))).transpose((1, 2, 0))
        assert x_y_offset.shape == (grid_size, grid_size, 2)
        # add empty dimension in middle for broadcasting.
        x_y_offset = x_y_offset.reshape((1, grid_size, grid_size, 1, 2))
        # convert np.array to tensor
        x_y_offset_tensor = tf.convert_to_tensor(x_y_offset, dtype=input_dtype)

        # add offset of x and y on out_put, and resize to input image's size.
        centre_x_y = (centre_x_y + x_y_offset_tensor) * stride

        """log shape transform height and width"""
        # add empty dimension in middle for broadcasting.
        # anchors = anchors.reshape((1, 1, 1, num_anchors, 2))
        # change the list of tuple which is size of anchor box to tensor
        anchors_tensor = tf.convert_to_tensor(anchors.reshape((1, 1, 1, num_anchors, 2)), dtype=input_dtype)

        # compute the height and width of bounding box to get real size based on output image size.
        bounding_box_size = tf.exp(new_prediction[..., 2:4]) * anchors_tensor

        """sigmoid object confidence"""
        objectness_score = tf.sigmoid(new_prediction[..., 4:5])  # sigmoid object confidence
        assert len(objectness_score.shape) == 5

        """apply sigmoid activation to the class scores."""
        classes_score = tf.sigmoid(new_prediction[..., 5:])

        # Concatenate bounding box info, objectness score and classes score together at axis=-1(channel axis) as
        # transformed prediction.
        transformed_prediction = tf.concat([centre_x_y, bounding_box_size, objectness_score, classes_score], axis=-1)
        assert transformed_prediction.get_shape().as_list() == new_prediction.get_shape().as_list()

        predict_output = tf.reshape(transformed_prediction, shape=predict_shape)

        # return transformed detection
        return predict_output

    return layer_function


def predict_transform_shape(num_anchors: int, num_classes: int):
    """
    get output tensor's shape for Lambda layer
    :param num_anchors:
    :param num_classes:
    :return:
    """

    def lambda_layer_shape_func(input_shape):
        # Compute output shape function.
        # input shape: B*H*W*C
        assert isinstance(input_shape, tuple)  # get type of Lambda's output_shape func
        assert len(input_shape) == 4  # input shape with Batch size

        # assume input_shape without batch size
        num_grid = input_shape[1]  # get the num of grid box
        assert num_grid == input_shape[2]  # check

        num_of_bounding_box_attributes = 5 + num_classes
        assert num_of_bounding_box_attributes * num_anchors == input_shape[-1]  # check

        new_shape = (num_grid * num_grid * num_anchors, num_of_bounding_box_attributes)

        return new_shape

    return lambda_layer_shape_func


def _darknet_53_block(input: tf.Tensor, filters: int):
    """ the structure of darknet 53 like inception-net

    :param input:  
    :param filters: the number of filter used in first layer of block
    :return:
    """

    mid_output = conv2d_with_bn(filters, kernel_size=1, stride=1, padding=1, activation="leaky", batch_normalize=1)(
        input)
    mid_output = conv2d_with_bn(filters * 2, kernel_size=3, stride=1, padding=1, activation="leaky", batch_normalize=1)(
        mid_output)

    layer_output = Add()([input, mid_output])

    return layer_output


def darknet_53(input: tf.Tensor):
    """ Implement darknet 53 network

    As YOLOv3 doesn't use global avg pool layer and softmax layer. in fact there
    are 52 layers instead of 53.
    implement it follow the tutorials:
    https://itnext.io/implementing-yolo-v3-in-tensorflow-tf-slim-c3c55ff59dbe

    :param input: A tensor from Input layer
    :return:
    """

    input_shape = input.get_shape().as_list()
    assert input_shape[1] == input_shape[2] == 416

    mid_output = conv2d_with_bn(32, kernel_size=3, stride=1, padding=1, activation="leaky", batch_normalize=1)(input)
    mid_output = conv2d_with_bn(64, kernel_size=3, stride=2, padding=1, activation="leaky",
                                batch_normalize=1)(mid_output)

    mid_output = _darknet_53_block(mid_output, filters=32)

    mid_output = conv2d_with_bn(128, kernel_size=3, stride=2, padding=1, activation="leaky",
                                batch_normalize=1)(mid_output)

    for _ in range(2):
        mid_output = _darknet_53_block(mid_output, filters=64)

    mid_output = conv2d_with_bn(256, kernel_size=3, stride=2, padding=1, activation="leaky",
                                batch_normalize=1)(mid_output)

    for _ in range(8):
        mid_output = _darknet_53_block(mid_output, filters=128)

    route1 = mid_output
    mid_output = conv2d_with_bn(512, kernel_size=3, stride=2, padding=1, activation="leaky",
                                batch_normalize=1)(mid_output)

    for _ in range(8):
        mid_output = _darknet_53_block(mid_output, filters=256)

    route2 = mid_output
    mid_output = conv2d_with_bn(1024, kernel_size=3, stride=2, padding=1, activation="leaky",
                                batch_normalize=1)(mid_output)

    for _ in range(4):
        mid_output = _darknet_53_block(mid_output, filters=512)

    return route1, route2, mid_output


def yolo_layer(input: tf.Tensor, filters: int):
    """ some convolution layers between darknet-53 and detection layer

    :param input:
    :param filters: the number of filter used in first layer of block
    :return:
    """

    for _ in range(2):
        input = conv2d_with_bn(filters, kernel_size=1, stride=1, padding=1, activation="leaky",
                               batch_normalize=1)(input)
        input = conv2d_with_bn(filters * 2, kernel_size=3, stride=1, padding=1, activation="leaky",
                               batch_normalize=1)(input)

    input = conv2d_with_bn(filters, kernel_size=1, stride=1, padding=1, activation="leaky", batch_normalize=1)(input)

    route = input

    input = conv2d_with_bn(filters * 2, kernel_size=3, stride=1, padding=1, activation="leaky", batch_normalize=1)(input)
    output = conv2d_with_bn(255, kernel_size=1, stride=1, padding=1, activation="linear", batch_normalize=0)(input)

    return route, output
