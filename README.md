# YOLO
This is only a exercise.

Implement a YOLO (v3) object detector in Keras and TensorFlow.

Follow the tutorial of:
1. How to implement a YOLO (v3) object detector from scratch in PyTorch<br />
   https://blog.paperspace.com/how-to-implement-a-yolo-object-detector-in-pytorch/
   
2. Implementing YOLO v3 in Tensorflow (TF-Slim)<br />
   https://itnext.io/implementing-yolo-v3-in-tensorflow-tf-slim-c3c55ff59dbe
