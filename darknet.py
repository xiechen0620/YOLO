"""
@article{yolov3,
  title={YOLOv3: An Incremental Improvement},
  author={Redmon, Joseph and Farhadi, Ali},
  journal = {arXiv},
  year={2018}
}
"""

from __future__ import division

import re
from contextlib import suppress

import numpy as np
from keras.layers import Input, Concatenate, Lambda, Add
from keras.models import Model
import cv2

from util import non_max_suppression, draw_bounding_box
from layer_function import conv2d_with_bn, bilinear_upsample, bilinear_upsample_shape, predict_decode
from layer_function import darknet_53, yolo_layer, reset_layer_counter


def parse_cfg(cfgfile: str) -> list:
    """ get layer info from config file.

    All attribute of layer in config file will described as key-value in layer's dict.
    Order of dicts in list is same as layers' order in config file.

    :param cfgfile: a string of the path of cfg file.

    :return: 
        block_list: A list of layers dict. Each dict store layer info by key-value pair.
    """

    # Open config file of network and read all lines.
    with open(cfgfile, 'r') as cfg_file:
        lines = cfg_file.readlines()

    block_list = []  # list for blocks
    net_dict = None  # block dict used for first loop

    # Traverse lines, get all layers and its attributes in one dict(net_dict) as a block.
    # then append net_dict into block_list if get another layer.
    for line in lines:

        if line[0] == "#" or line == "\n":
            # check for comments and empty line
            continue

        # get attributes from the readline.
        # As attributes part after block type, this part should follow
        # the block type part. But there are much more line of attributes than 
        # block type's, so I put it before the block type check wish could make
        # the code more efficient.
        match = re.search(r'(?P<key>\w+)(?: ?)=(?: ?)(?P<value>[\w.,\- ]+)', line)
        if match:
            # As the first line of config file is block type, this error would not be
            # touched when the first loop.
            # But it still there, still working on it.
            net_dict[match.group("key")] = match.group("value")
            # As this line describe the attribute of layer, 
            # no need to match the layer type again.
            continue

        # check for layer type.
        match = re.search(r'\[(?P<netType>\w+)\]', line)
        if match:
            if net_dict:
                # if it is not the first loop, put the dict in the 
                # end of block_list; and skip when it is first loop.
                block_list.append(net_dict)
            net_dict = {"type": match.group("netType")}

            # #As this line describe the type of layer,
            # # no need to match attribute in this again.
            # continue       
    else:
        # When finish traverse of lines, the last layer dict doesn't append into
        # block_list. So do it at here.
        block_list.append(net_dict)

    return block_list


def load_weights(weight_file: str, model: Model, conv_index: int):
    """ Load weight from weight_file into model.

    The conv and BN layer weight's shape which getting by layer's name, as size of data getting from weight_file.
    Then, reset layer weight of conv and BN layer.

    :param weight_file: string of weights file path.
    :param model: Keras model which load weights into.
    :param conv_index: the number of convolution layers in model.

    :return: nothing.
    """

    with open(weight_file, "rb") as fp:
        # The first 5 values are header information
        # 1. Major version number
        # 2. Minor version number
        # 3. subversion number
        # 4,5. Images seen by the network (during training)
        _ = np.fromfile(fp, dtype=np.int32, count=5)  # read first 5 values
        # seen = header[3]

        # read weights from file for each layer.
        for index in range(conv_index):
            conv_layer = model.get_layer(name="conv2d_" + str(index + 1))  # get conv layer by order.
            conv_weights = conv_layer.get_weights()  # get layer's weights

            # check there  is a bn_layer after conv_layer or not.
            try:
                # if could get bn_layer with same index with conv_layer, then read bn layer weights first.
                bn_layer = model.get_layer(name="batch_normalization_" + str(index + 1))
                bn_weights = bn_layer.get_weights()

                print("loading weight of: " + bn_layer.name)  # print info

                bn_weight_size = bn_weights[0].size
                bn_weight_shape = bn_weights[0].shape

                # do some check about weight list of BN and Conv layer.
                assert len(bn_weight_shape) == 1
                assert all([weight.shape == bn_weight_shape for weight in bn_weights])
                assert len(conv_weights) == 1

                # load BN layer weights
                bn_bias = np.fromfile(fp, dtype=np.float32, count=bn_weight_size)
                bn_weight = np.fromfile(fp, dtype=np.float32, count=bn_weight_size)
                bn_running_mean = np.fromfile(fp, dtype=np.float32, count=bn_weight_size)
                bn_running_var = np.fromfile(fp, dtype=np.float32, count=bn_weight_size)

                # Keras batch normalization's weight order is [gamma, beta, mean, std]
                # gamma is equal to weight, and beta equal to bias.
                # https://stackoverflow.com/questions/42793792/how-to-set-weights-of-the-batch-normalization-layer
                bn_weights = [bn_weight, bn_bias, bn_running_mean, bn_running_var]

                bn_layer.set_weights(bn_weights)

                conv_bias = None

            except ValueError:
                # If a Conv layer doesn't followed by a BN layer, it means this Conv layer's use_bias
                # parameter would be 1. So, load bias weight first, then convolution weight.
                print("loading bias weight of: " + conv_layer.name)
                assert len(conv_weights) == 2

                conv_bias_shape = conv_weights[1].shape
                assert len(conv_bias_shape) == 1
                conv_bias_size = conv_weights[1].size

                conv_bias = np.fromfile(fp, dtype=np.float32, count=conv_bias_size)

            print("loading convolution weight of: " + conv_layer.name)
            conv_weight_size = conv_weights[0].size
            conv_weight_shape = conv_weights[0].shape

            # according to this tutorial, read convolution weights from file and reshape to tf type
            # https://itnext.io/implementing-yolo-v3-in-tensorflow-tf-slim-c3c55ff59dbe
            conv_weight = np.fromfile(fp, dtype=np.float32, count=conv_weight_size)
            conv_weight = np.reshape(conv_weight, newshape=(conv_weight_shape[3], conv_weight_shape[2],
                                                            conv_weight_shape[1], conv_weight_shape[0]))
            conv_weight = np.transpose(conv_weight, (2, 3, 1, 0))

            conv_weights = [conv_weight] if conv_bias is None else [conv_weight, conv_bias]

            conv_layer.set_weights(conv_weights)

        else:
            print("Loading weight file succeed")


def create_model_list(blocks: list):
    """ Build each layer by blocks list.

    As create model by block_list directly would leave may variables which don't need for model,
    I define this function to build a list of layer.

    Here, I build the model using Function API instead of Sequential model for these reason:
        1. In offical code of YOLO v3 of Darknet, a Conv layer followed by a BN layer is treated
            as a single Conv layer with BN, and a Conv layer only is treated as a single Conv layer
            without BN. But in Keras, Conv layer is one layer and BN is other layer.
            If using Sequential Model, it is difficult to keep layer number of Keras Model
            consistent with offical model.
            It's easy to get wrong in route layer and shortcut layer.

            So, I use Function API to keep order of blocks in Keras consistent with offical model.
            And it seems like easy to build route layer and shortcut layer.

        2. For now, the output of model has three output for three scales. May be I could put them
            together in one main_output by FC layer or something else, but compute map will has
            three route for three scales.

            so, I had to use Function API, unless put all three scales part in one Function model
            as a layer of Sequential Model. I'll try this later.
         
    In layer list, Conv layer and upsample layer are functional layers which could put in model directly with a input;
    route layer and shortcut layer need other layers' output as input, so they have to be created when building model;
    yolo layer is unfinished yet.
    So route layer, shortcut layer and yolo layer are dict in block_list, the functional layer
    would be created when building the model.

    :param blocks: layer list which element is dict. The layer info from cfg_file.
    
    :return: 
        net_info: a dict store info of model. e.g. number of classes, learining rate.
        model_list: a list hold layers which is layer or a dict.
        hold_output_set: a set stores all layers index need to save temporary for route layer or shortcut layer
        conv_index: count the number of convolution layers when building model.
    """

    # Captures the information about the input and pre-processing
    net_info = blocks[0]
    model_list = []  # list of layer_func
    # conv_index = 0  # count the number of conv layer

    # The output of layers which index in this list should be hold for route layer or shortcut layer.
    hold_output_set = set()

    # As first element of block_list(blocks[0]) stores net_info, traverse block_list[1:], 
    # bulid layer func or layer dict for each layer. Then append it into model_list. 
    for index, block in enumerate(blocks[1:]):
        # Get attributes for each layer
        if block["type"] == "convolutional":
            # It's a conv layer
            # conv_index += 1  # count number of this conv layer

            # check this conv layer with or without BN layer.
            try:
                batch_normalize = int(block["batch_normalize"])
            except KeyError:
                batch_normalize = 0

            filters = int(block["filters"])
            kernel_size = int(block["size"])
            stride = int(block["stride"])
            padding = int(block["pad"])
            activation = block["activation"]

            layer = conv2d_with_bn(filters, kernel_size, stride, padding, activation, batch_normalize)

        elif block["type"] == "upsample":
            # If it's a upsampling layer, we use Bilinear2dUpsampling.
            stride = int(block["stride"])

            layer = Lambda(bilinear_upsample(stride=stride), output_shape=bilinear_upsample_shape(stride=stride),
                           name="bilinear_upsample_" + str(index))

        elif block["type"] == "route":
            # create route layer
            link_layers = block["layers"].split(',')
            start = int(link_layers[0])  # set Start of route

            # Positive annotation.
            # Change relative layer number to absolute number start from beginning.
            start = start + index if start < 0 else start

            # Put necessary info in dict as route layer
            # Note: start layer and end layer(if exist) are using positive value.
            layer = {"layerType": "route", "start": start}
            hold_output_set.add(start)

            # set end of route, if there exists one.
            with suppress(IndexError):
                end = int(link_layers[1])

                # Positive annotation.
                # Change relative layer number to absolute number start from beginning.
                end = end + index if end < 0 else end

                layer["end"] = end  # add end info to route layer.
                hold_output_set.add(end)

        elif block["type"] == "shortcut":
            # create shortcut layer
            from_index = int(block["from"]) + index

            layer = {"layerType": "shortcut", "from": from_index, "activation": block["activation"]}
            hold_output_set.add(from_index)

        elif block["type"] == "yolo":
            # create detection layer, which is yolo layer in darknet framework.
            mask = block["mask"].split(',')
            mask = [int(x) for x in mask]  # get masks of activating anchors

            anchors = block["anchors"].split(',')  # get all anchors
            # rebuild the anchor size to tuples
            anchors = np.int32([(anchors[i], anchors[i + 1]) for i in range(0, len(anchors), 2)])
            activating_anchors = anchors[mask]
            assert type(activating_anchors) == np.ndarray

            layer = {"layerType": "yolo", "anchors": activating_anchors, "classes": int(block["classes"])}

        else:
            # miss match all layer types.
            # It shouldn't happen.
            continue

        model_list.append(layer)

    layer_number = reset_layer_counter()

    return net_info, model_list, hold_output_set, layer_number


def create_yolo_model(net_info: dict, model_list: list, hold_output_set: set, input_shape=(416, 416, 3)) -> Model:
    """ Create the model according to layers' list.

    :param net_info: a dict store the setting of model.
    :param model_list: A list of layers.
    :param hold_output_set: the layer index to store output temporary for route or shortcut.
    :param input_shape: shape of input image without batch.
    
    :return: 
        model: A functional Keras model
    """

    # Define the input as a tensor with shape input_shape
    # input data shape should be: (?, 416, 416, 3)
    input_layer = Input(input_shape)
    layer_output = input_layer  # temporary variable. Used fro recursive.
    detections = []  # Temporary variable. Get all three detection layers' output.
    layer_output_dic = {}  # Catch the output of each layer used for route layer and shortcut layer.

    # Traverse the model_list
    for layer_index, layer in enumerate(model_list):
        # In model_list, both convNet layer and upsample layer are functional,
        # to let layer work, just need a input.
        # But route and shortcut layer, they need the output from other layers,
        # so, when create model_list, this two layers set as dict to hold info,
        # when building the model, get info from dict of layer, and get output
        # from layer_output_list to implement route or shortcut layer.
        layer_type = type(layer)  # get layer type
        if layer_type != dict:
            # This layer is  convolution or upsampling layer.
            layer_output = layer(layer_output)  # use layer directly.

        else:
            # This layer is route or shortcut or yolo layer.
            if layer["layerType"] == "route":  # This layer is route layer.
                route_start = layer["start"]  # get start layer index. Positive value
                map_start = layer_output_dic[route_start]  # get output tensor from start layer.

                try:
                    # try to get end layer index. If exist, get the index and output tensor of that layer.
                    route_end = layer["end"]  # Positive value
                    map_end = layer_output_dic[route_end]

                    assert map_start.get_shape().as_list()[:-1] == map_end.get_shape().as_list()[:-1]

                    # out put of route layer is concatenate two layers' output tensor at channel axis.
                    layer_output = Concatenate(axis=-1)([map_start, map_end])

                except KeyError:
                    # If there is only start layer, the output tensor is start layer's output.
                    layer_output = map_start

            elif layer["layerType"] == "shortcut":  # This layer is shortcut layer.
                shortcut_from = layer["from"]  # get add layer's index

                layer_a = layer_output
                layer_b = layer_output_dic[shortcut_from]

                assert layer_a.get_shape().as_list() == layer_b.get_shape().as_list()

                # the sum of from_layer and previous layer as shortcut layer's output.
                layer_output = Add()([layer_a, layer_b])

            elif layer["layerType"] == "yolo":  # yolo layer
                anchors = layer["anchors"]  # get anchors' size

                inp_dim = int(net_info["height"])  # Get input image dimension
                num_classes = layer["classes"]  # Get the number of classes

                # Transfer convNet output value to coordinate on image and probability.
                layer_output = Lambda(predict_decode(inp_dim, anchors, num_classes))(layer_output)

                detections.append(layer_output)

            else:
                # error
                continue

        if layer_index in hold_output_set:  # append output in list
            layer_output_dic[layer_index] = layer_output

    model = Model(inputs=input_layer, outputs=detections, name='YOLO')

    return model


def build_model_use_darknet53(input_shape=(416, 416, 3)):
    """ Implement YOLOv3 by darknet-53 net

    create model use darknet-53 network introduced in paper(not use config file)

    :param input_shape: a tuple as shape of input picture
    :return:
    """
    inp_dim = 416
    num_classes = 80
    anchors = np.array([(10, 13), (16, 30), (33, 23), (30, 61), (62, 45), (59, 119), (116, 90), (156, 198), (373, 326)])

    input_layer = Input(input_shape)

    route1, route2, output_of_darknet53 = darknet_53(input_layer)

    # grid: 13*13
    route, output_of_yolo1 = yolo_layer(output_of_darknet53, 512)
    detection1 = Lambda(predict_decode(inp_dim, anchors[6:], num_classes))(output_of_yolo1)

    mid_output = conv2d_with_bn(256, kernel_size=1, stride=1, padding=1, activation="leaky", batch_normalize=1)(route)
    mid_output = Lambda(bilinear_upsample(stride=2), output_shape=bilinear_upsample_shape(stride=2))(mid_output)
    mid_output = Concatenate(axis=-1)([mid_output, route2])

    # grid: 26*26
    route, output_of_yolo2 = yolo_layer(mid_output, 256)
    detection2 = Lambda(predict_decode(inp_dim, anchors[3:6], num_classes))(output_of_yolo2)

    mid_output = conv2d_with_bn(128, kernel_size=1, stride=1, padding=1, activation="leaky", batch_normalize=1)(route)
    mid_output = Lambda(bilinear_upsample(stride=2), output_shape=bilinear_upsample_shape(stride=2))(mid_output)
    mid_output = Concatenate(axis=-1)([mid_output, route1])

    # grid: 52*52
    _, output_of_yolo3 = yolo_layer(mid_output, 128)
    detection3 = Lambda(predict_decode(inp_dim, anchors[0:3], num_classes))(output_of_yolo3)

    model = Model(inputs=input_layer, outputs=[detection1, detection2, detection3], name="YOLOv3_darknet53")

    layer_number = reset_layer_counter()

    return model, layer_number


def get_images(img_paths: list) -> np.ndarray:
    """ Load images by batch and resize them.

    :param img_paths: a list of image paths for predict or detect.
    
    :return:
        imgs_resized: a array of resized images by batch.
    """
    # img_array = list(map(cv2.imread, img_paths))  # shape: B*H*W*C channel: BGR
    img_array = map(cv2.imread, img_paths)  # shape: B*H*W*C channel: BGR

    # shape: B*H*W*C, channel: BGR
    imgs_resized = np.array(list(map(lambda image: cv2.resize(image, (416, 416)), img_array)))
    imgs_resized = imgs_resized[..., ::-1] / 255  # change channel order to RGB, and normalize images.

    assert len(imgs_resized.shape) == 4
    assert imgs_resized.dtype == np.float64
    assert imgs_resized.shape[0] == len(img_paths)

    # output should be a array which shape should be: B*H*W*C, and order of channel should be RGB
    return imgs_resized


def load_classes(names_file: str) -> list:
    with open(names_file, 'r') as fp:
        names = fp.read().split("\n")[:-1]

    return names


def main():
    config_file_path = "./cfg/yolov3.cfg"
    image_paths = ["./img/test.png", "./img/dog-cycle-car.png", "./img/BingWallpaper-2017-11-04.jpg"]

    num_classes = 80  # For COCO
    classes: list = load_classes("./data/coco.names")
    assert len(classes) == num_classes

    # image_paths = ["./img/test.png"]
    my_list = parse_cfg(config_file_path)
    net_info, model_list, hold_output_set, conv_index = create_model_list(my_list)
    yolo_model = create_yolo_model(net_info, model_list, hold_output_set)
    # yolo_model, conv_index = build_model_use_darknet53()
    # plot_model(yolo_model, to_file='./model2.png', show_shapes=True)
    load_weights(weight_file="./weights/yolov3.weights", model=yolo_model, conv_index=conv_index)
    yolo_model.compile(optimizer="Adam", loss="mean_squared_error")
    yolo_model.summary()

    test_img = get_images(image_paths)
    test = yolo_model.predict(test_img)

    results = non_max_suppression(test, confidence=0.5, num_classes=num_classes)
    print(results)

    draw_bounding_box(results, image_paths, classes)


if __name__ == '__main__':
    main()
