import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


def exercise1():
    x = np.linspace(-np.pi, np.pi, num=256)
    c, s = np.cos(x), np.sin(x)

    plt.plot(x, c)
    plt.plot(x, s)

    plt.show()


def exercise2():
    plt.figure(figsize=(8, 6), dpi=80)
    plt.subplot(111)

    x = np.linspace(-np.pi, np.pi, num=256)
    c, s = np.cos(x), np.sin(x)

    # print sin and cos
    plt.plot(x, c, color="blue", linewidth=1.0, linestyle="-")
    plt.plot(x, s, color="green", linewidth=1.0, linestyle="dotted")

    plt.xlim(-4.0, 4.0)
    plt.xticks(np.linspace(-4.0, 4.0, 9))

    plt.ylim(-1.0, 1.0)
    plt.yticks(np.linspace(-1.0, 1.0, 5))

    plt.show()


def exercise3():
    x = np.linspace(-np.pi, np.pi, 256)
    c, s = np.cos(x), np.sin(x)

    plt.plot(x, c, color="blue", linewidth=1.0, linestyle="-")
    plt.plot(x, s, color="green", linewidth=1.0, linestyle="dotted")

    plt.xticks(np.linspace(-np.pi, np.pi, 5), [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'])
    plt.yticks([-1.0, 0, 1.0])

    plt.show()


def exercise4():
    x = np.linspace(-np.pi, np.pi, 256)
    c, s = np.cos(x), np.sin(x)

    ax = plt.gca()
    ax.spines["right"].set_color("none")
    ax.spines["top"].set_color("none")
    ax.xaxis.set_ticks_position("bottom")
    ax.spines["bottom"].set_position(("data", 0))
    ax.yaxis.set_ticks_position("left")
    ax.spines["left"].set_position(("data", 0))

    plt.plot(x, c, color="blue", linewidth=1.0, linestyle="-")
    plt.plot(x, s, color="green", linewidth=1.0, linestyle="dotted")

    plt.xticks(np.linspace(-np.pi, np.pi, 5), [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'])
    plt.yticks([-1.0, 0, 1.0])

    plt.show()


def exercise5():
    x = np.linspace(-np.pi, np.pi, 256)
    c, s = np.cos(x), np.sin(x)

    ax = plt.gca()
    ax.spines["right"].set_color("none")
    ax.spines["top"].set_color("none")
    ax.spines["bottom"].set_position(("data", 0))
    ax.spines["left"].set_position(("data", 0))
    ax.xaxis.set_ticks_position("bottom")
    ax.yaxis.set_ticks_position("left")

    plt.plot(x, c, color="blue", linewidth=1.0, linestyle="-", label="cos")
    plt.plot(x, s, color="red", linewidth=1.0, linestyle="dotted", label="sin")

    plt.xticks(np.linspace(-np.pi, np.pi, 5), [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'])
    plt.yticks([-1.0, 0, 1.0])

    plt.legend(loc="upper left", frameon=True)

    plt.show()


def exercise6():
    x = np.linspace(-np.pi, np.pi, 256)
    c, s = np.cos(x), np.sin(x)

    ax = plt.gca()
    ax.spines["right"].set_color("none")
    ax.spines["top"].set_color("none")
    ax.spines["bottom"].set_position(("data", 0))
    ax.spines["left"].set_position(("data", 0))
    ax.xaxis.set_ticks_position("bottom")
    ax.yaxis.set_ticks_position("left")

    plt.plot(x, c, color="blue", linewidth=2.5, linestyle="-", label="cos")
    plt.plot(x, s, color="red", linewidth=2.5, linestyle="-", label="sin")

    plt.xticks(np.linspace(-np.pi, np.pi, 5), [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'])
    plt.yticks([-1.0, 0, 1.0])

    plt.legend(loc="upper left", frameon=True)

    t = 2 * np.pi / 3

    plt.plot([t, t], [0, np.cos(t)], color="blue", linewidth=1.5, linestyle="--")
    plt.scatter([t, ], [np.cos(t), ], 50, color="blue")

    plt.annotate(r'$\cos(\frac{2\pi}{3})=\frac{1}{2}$', 
                xy=(t, np.cos(t)), xycoords="data", xytext=(-90, -50), textcoords="offset points", 
                fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3, rad=.2"))

    plt.plot([t, t], [0, np.sin(t)], color="red", linewidth=1.5, linestyle="--")
    plt.scatter([t, ], [np.sin(t), ], 50, color="red")

    plt.annotate(r'$\sin(\frac{2\pi}{3})=\frac{\sqrt{3}}{2}$',
                xy=(t, np.sin(t)), xycoords="data", xytext=(+10, +30), textcoords="offset points", 
                fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3, rad=.2"))

    plt.show()


def exercise7():
    x = np.linspace(-np.pi, np.pi, 256)
    c, s = np.cos(x), np.sin(x)

    ax = plt.gca()
    ax.spines["right"].set_color("none")
    ax.spines["top"].set_color("none")
    ax.spines["bottom"].set_position(("data", 0))
    ax.spines["left"].set_position(("data", 0))
    ax.xaxis.set_ticks_position("bottom")
    ax.yaxis.set_ticks_position("left")

    plt.plot(x, c, color="blue", linewidth=2.5, linestyle="-", label="cos")
    plt.plot(x, s, color="red", linewidth=2.5, linestyle="-", label="sin")

    plt.xticks(np.linspace(-np.pi, np.pi, 5), [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'])
    plt.yticks([-1, 1], [r'$-1.0$', r'$1.0$'])

    plt.legend(loc="upper left", frameon=True)

    t = 2 * np.pi / 3

    plt.plot([t, t], [0, np.cos(t)], color="blue", linewidth=1.5, linestyle="--")
    plt.scatter([t, ], [np.cos(t), ], 50, color="blue")

    plt.annotate(r'$\cos(\frac{2\pi}{3})=\frac{1}{2}$', 
                xy=(t, np.cos(t)), xycoords="data", xytext=(-90, -50), textcoords="offset points", 
                fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3, rad=.2"))

    plt.plot([t, t], [0, np.sin(t)], color="red", linewidth=1.5, linestyle="--")
    plt.scatter([t, ], [np.sin(t), ], 50, color="red")

    plt.annotate(r'$\sin(\frac{2\pi}{3})=\frac{\sqrt{3}}{2}$',
                xy=(t, np.sin(t)), xycoords="data", xytext=(+10, +30), textcoords="offset points", 
                fontsize=16, arrowprops=dict(arrowstyle="->", connectionstyle="arc3, rad=.2"))

    for label in ax.get_xticklabels() + ax.get_yticklabels():
        label.set_fontsize(16)
        label.set_bbox(dict(facecolor="white", edgecolor="None", alpha=0.65))

    plt.show()


def exercise8():
    """Drip drop"""
    fig = plt.figure(figsize=(6, 6), facecolor="white")
    ax = fig.add_axes([0, 0, 1, 1], frameon=False, aspect=1)

    n = 50
    size_min = 50
    size_max = 50 * 50

    points = np.random.uniform(0, 1, (n, 2))
    colors = np.zeros((n, 4))
    colors[:, 3] = np.linspace(0, 1, n)

    ring_size = np.linspace(size_min, size_max, n)

    scat = ax.scatter(points[:, 0], points[:, 1], s=ring_size, lw=0.5, 
                        edgecolor=colors, facecolor="None")
    
    ax.set_xlim(0, 1)
    ax.set_xticks([])
    ax.set_ylim(0, 1)
    ax.set_yticks([])

    def update(frame):
        nonlocal ring_size

        colors[:, 3] = np.maximum(0, colors[:, 3] - 1.0/n)
        ring_size += (size_max -size_min) / n

        # Reset ring specific ring (relative to frame number)
        i = frame % 50
        points[i, :] = np.random.uniform(0, 1, 2)
        ring_size[i] = size_min
        colors[i, 3] = 1

        # Update scatter object
        scat.set_edgecolors(colors)
        scat.set_sizes(ring_size)
        scat.set_offsets(points)

        return scat

    animation = FuncAnimation(fig, update, interval=10, frames=200)
    plt.show()


def exercise9():
    """scatter"""
    n = 512
    x = np.random.normal(0, 1, n)
    y = np.random.normal(0, 1, n)
    colors = np.arctan2(y, x)

    plt.scatter(x, y, s=75, c=colors, alpha=.5)

    plt.show()


def main():
    exercise9()

if __name__ == '__main__':
    main()